import { createSlice } from "@reduxjs/toolkit";
import { allRecipes } from "../thunks/recipeThunk";

export const recipeSlice = createSlice({
    name: 'recipe',
    initialState: {
        recipes: [],
        status: 'pending',
        error: null,
        message: ''
    },
    extraReducers: {
        [allRecipes.fulfilled]: (state, action) => {
            if (action.payload?.message) {
                state.status = 'error'
                state.message = action.payload?.message
                return
            }
            state.status = 'fulfilled';
            state.recipes = action.payload;
        },
        [allRecipes.pending]: (state, action) => {
            state.status = "error";
            state.error = true;
        },
        [allRecipes.rejected]: (state, action) => {
            state.status = 'loading'
        }
    }
})

export default recipeSlice.reducer;