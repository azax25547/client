import { createSlice } from "@reduxjs/toolkit";
import { loginUser } from "../thunks/authThunk";

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: '',
        status: 'pending',
        error: null,
        message: ''
    },
    reducers: {

    },
    extraReducers: {
        [loginUser.fulfilled]: (state, action) => {
            if (action.payload?.message) {
                state.status = 'error'
                state.message = action.payload?.message

                return
            }
            state.status = 'fulfilled';
            state.user = action.payload;
        },
        [loginUser.rejected]: (state, action) => {
            state.status = "pending";

        },
        [loginUser.pending]: (state, action) => {
            state.status = 'loading'
        },

    }
})

export default authSlice.reducer;