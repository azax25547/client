import { createSlice } from "@reduxjs/toolkit";
import { getSingleUserDetails } from "../thunks/userThunk";

export const userSlice = createSlice({
    name: "user",
    initialState: {
        userData: [],
        status: 'pending',
    },
    extraReducers: {
        [getSingleUserDetails.fulfilled]: (state, action) => {
            if (action.payload?.message) {
                state.status = 'error'
                return;
            }
            state.status = 'fulfilled';
            state.userData = action.payload;
        },
        [getSingleUserDetails.pending]: (state, action) => {
            state.status = "pending";
        }
    }
})


export default userSlice.reducer;