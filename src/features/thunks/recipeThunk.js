import { createAsyncThunk } from "@reduxjs/toolkit";
import { getAllRecipes } from "../../services/recipeService";

export const allRecipes = createAsyncThunk(
    'recipe/all',
    async (_, thunkAPI) => {
        try {
            const response = await getAllRecipes();
            if (response.status === 200)
                return response.data;

            else
                return thunkAPI.rejectWithValue([]);
        } catch (err) {
            return err.response.data;
        }
    })