import { createAsyncThunk } from "@reduxjs/toolkit";
import { login } from "../../services/authService";

export const loginUser = createAsyncThunk('auth/login', async ({ email, password }, thunkAPI) => {
    try {
        const response = await login({
            username: email,
            password: password
        })
        if (response.status === 200) {
            return response.data;
        }

        else {
            return thunkAPI.rejectWithValue([]);
        }

    } catch (err) {
        return (err.response.data);
    }

})
