import { createAsyncThunk } from "@reduxjs/toolkit";
import { getASingleUser } from "../../services/userService";

export const getSingleUserDetails = createAsyncThunk(
    'user/single',
    async ({ id }, thunkApi) => {
        try {
            const response = await getASingleUser(id);
            if (response.status === 200)
                return response.data;

            else
                return thunkApi.rejectWithValue([]);
        } catch (err) {
            return err.response.data;
        }
    }
)