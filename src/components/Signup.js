import { useState } from "react";
import { useHistory } from "react-router";
import { signUp } from "../services/authService";
import { Link } from "react-router-dom";

const Signup = () => {

    const [fname, setFname] = useState('')
    const [lname, setLname] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [cfPassword, setCfPassword] = useState('')
    const [errors, setErrors] = useState('');
    const history = useHistory();

    const removeTheAlert = () => {
        setTimeout(() => setErrors(''), 2000)
    }

    const submitHandler = async (e) => {
        e.preventDefault();
        const response = await
            signUp({
                "firstname": fname,
                "lastname": lname,
                "username": email,
                'password': password,
                "contact": phone
            })

        if (response.status === 201) {
            alert("Sign Up Successful")
            history.push('/login')
        }
        else
            alert("We'll se");
    }


    return (
        <div className="container mt-3 px-5 py-5">
            <span className="display-4">SIGN UP</span>
            <form
                className='needs-validation row p-5'
                onSubmit={submitHandler}

            >
                <div className=' col-md-6 col-sm-12'>
                    <label htmlFor="firstName" className="form-label">First Name</label>
                    <input className="form-control" placeholder="Your first name" value={fname} onChange={(e) => setFname(e.target.value)} required />
                    <div className="invalid-feedback">
                        First name Please!
                    </div>
                </div>
                <div className=" col-md-6 col-sm-12">
                    <label htmlFor="lastName" className="form-label">Last Name</label>
                    <input className="form-control" label="Last Name" placeholder="Your last name" value={lname} onChange={(e) => setLname(e.target.value)} required />
                    <div className="invalid-feedback">
                        Last Name Please!
                    </div>
                </div>
                <div className="col-12">
                    <label htmlFor="phone" className="form-label">Contact Number</label>

                    <input className="form-control" placeholder="Contact Number" type="number" value={phone} onChange={(e) => setPhone(e.target.value)} required />
                    <div className="invalid-feedback">
                        Please provide a Phone Number
                    </div>
                </div>
                <div className="col-12">
                    <label htmlFor="email" className="form-label">Email</label>

                    <input className="form-control" placeholder="Your Email Address" type="email" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    <div className="invalid-feedback">
                        Please Enter an email Address
                    </div>
                </div>
                <div className="col-6">
                    <label htmlFor="password" className="form-label">Password</label>

                    <input className="form-control" placeholder="Password" type="password" value={password} onChange={(e) => {
                        setPassword(e.target.value)
                        if (password.length > 5 && password.length < 8) {
                            setErrors('Password must be greater than 8 characters');
                            removeTheAlert();
                        } else
                            setErrors('')
                    }
                    } required />
                    <div className="invalid-feedback">
                        Please Provide a Password
                    </div>
                </div>
                <div className="col-6">
                    <label htmlFor="confirmPassowrd" className="form-label">Confirm Password</label>

                    <input className="form-control" label="Confirm Password" placeholder="Confirm Password" type="password" value={cfPassword} onChange={(e) => {
                        setCfPassword(e.target.value)
                        if (cfPassword !== password) {
                            setErrors('Password is not matching. Please check the password')
                            removeTheAlert();
                        } else {
                            setErrors('')
                        }
                    }
                    } required />
                    <div className="invalid-feedback">
                        Password is not matching
                    </div>
                </div>
                <div className="col-12">

                    <button className="btn btn-primary my-4" type='submit' >Submit</button>
                </div>
            </form>

            Already have an Account. <Link to={"/login"}> Login </Link>

            {errors && (<div className="alert alert-danger" role="alert">
                {errors}
            </div>)}

        </div>

    );
}

export default Signup;