import { Link } from 'react-router-dom';

const Recipes = ({ recipes }) => {
    return (
        <>
            <section className='container py-2'>
                <h1 className='text-xl font-bold mb-8'>All Items</h1>

                <div className="row xs-3 md-3 lg-3 m-2 p-2">
                    {recipes && recipes.map((pizza, index) => {
                        return (
                            <div className="col p-2" key={index}>
                                <div className="card" style={{ width: '20rem' }}>
                                    <div className="card-body">
                                        <h5 className="card-title">
                                            <Link to={`/recipes/${pizza.id}`} className="text-decoration-none">{pizza.recipe}</Link>
                                        </h5>
                                        <p className="card-text">{pizza.recipeDescription}</p>
                                        <p className="card-subtitile text-muted">₹ {pizza.price}</p>
                                        <div className="text">{pizza.rating} out of 5 </div>

                                    </div>

                                </div>
                            </div>

                        )
                    })
                    }
                </div>

            </section>
        </>
    )
}

export default Recipes;