import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getOrders } from "../services/orderService";


const Order = () => {

    const { user, status: userStatus } = useSelector(state => state.auth);
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        const getUserOrders = async () => {
            if (user?.user && userStatus === "fulfilled") {

                const response = await getOrders(user.user._id);
                if (response.status === 200)
                    setOrders(response?.data);

            }
        }

        getUserOrders();
    }, [])
    const allOrders = orders.length > 0 ? orders.map(({ status, recipe }, index) => {
        return (
            <li className="list-group-item" key={index}>
                {/* list group item*/}
                {/* Custom content*/}
                <div className="media align-items-lg-center flex-column flex-lg-row p-3">
                    <div className="media-body order-2 order-lg-1">
                        <h5 className="mt-0 font-weight-bold mb-2">{recipe.recipe}</h5>
                        <p className="font-italic text-muted mb-0 small">
                            {recipe.recipeDescription}
                        </p>
                        <div className="d-flex align-items-center justify-content-between mt-1">
                            <h6 className="font-weight-bold my-2">Current Status : {status} </h6>
                            {/* <ul className="list-inline small">
                                    <li className="list-inline-item m-0">
                                        <i className="fa fa-star text-success" />
                                    </li>
                                    <li className="list-inline-item m-0">
                                        <i className="fa fa-star text-success" />
                                    </li>
                                    <li className="list-inline-item m-0">
                                        <i className="fa fa-star text-success" />
                                    </li>
                                    <li className="list-inline-item m-0">
                                        <i className="fa fa-star text-success" />
                                    </li>
                                    <li className="list-inline-item m-0">
                                        <i className="fa fa-star-o text-gray" />
                                    </li>
                                </ul> */}
                        </div>
                    </div>
                    <img
                        src={recipe.img.length > 0 ? recipe.img[0] : "https://images.pexels.com/photos/1653877/pexels-photo-1653877.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"}
                        alt="Generic placeholder image"
                        width={100}
                        className="ml-lg-5 order-1 order-lg-2"
                    />
                </div>
                {/* End */}
                {/* End */}
            </li>
        )
    }) : "No Orders"
    console.log(orders);
    return (
        <div className="container">
            <div className="row p-4">
                <h2>My Current Orders</h2>

            </div>
            <div className="row p-4">
                <h2 className="text-center">My Previous Orders</h2>
                <ul className="list-group shadow">
                    {allOrders}
                </ul>
            </div>

        </div>
    )
}

export default Order;