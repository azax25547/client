import axios from 'axios';
import React from 'react'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';

function Navbar({ user }) {

    const history = useHistory();

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">OG Brand</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <Link to='/' className="text-decoration-none nav-item text-secondary mt-1" style={{ marginRight: 10 }}>Home</Link>
                            {
                                user ?
                                    <>

                                        <Link to='/orders' className="text-decoration-none nav-item text-secondary mt-1" style={{ marginRight: 10 }} >My Orders</Link>
                                        <Link to='/' className="text-decoration-none nav-item text-secondary mt-1" style={{ marginRight: 10 }} onClick={() => {
                                            localStorage.clear();
                                            window.location.reload();
                                            window.location.assign("/")
                                            axios.get('/auth/logout').then(res => {

                                                if (res.status) {
                                                    console.log(res.status, 'Logged out Successfully.')


                                                }
                                            }).catch(err => console.error(err))
                                        }}>
                                            Logout

                                        </Link>
                                        {user?.user?.isAdmin ?
                                            <Link to='/dashboard' className="text-decoration-none nav-item text-secondary mt-1" style={{ marginRight: 10 }}><strong>DASHBOARD</strong></Link>
                                            : ""}
                                    </> :
                                    ""
                            }



                        </ul>
                        {user ? <div className='d-flex'>
                            <Link to={`/profile/${user?.user?._id}`} className="text-decoration-none nav-item text-secondary mt-1" style={{ marginRight: 10 }}>{`${user?.user?.firstname} ${user?.user?.lastname}`}</Link>

                        </div>
                            :
                            <div className='button-group'>
                                <Link to='/signup'><button className='btn btn-secondary p-1 mx-1'>SIGN UP</button></Link>
                                <Link to='/login'><button className='btn btn-secondary p-1 mx-1 '>LOG IN</button></Link>
                            </div>
                        }

                    </div>
                </div>
            </nav>

        </div>
    )
}

export default Navbar
