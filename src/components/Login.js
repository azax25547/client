import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { loginUser } from "../features/thunks/authThunk";

const Login = () => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [wrongPasswordCount, setWrongPasswordCount] = useState(0);
    const history = useHistory();
    const { error, status, user, message } = useSelector((state) => state.auth);


    const dispatch = useDispatch();

    useEffect(() => {
        if (status === 'fulfilled') {
            localStorage.setItem('token', user?.user?.token);
            history.push('/');
        } else if (status === 'loading')
            console.log("Loading...");
        else if (status === 'rejected' && error)
            console.log("Error")
        else if (status === 'error' && message) {
            alert(message)
            setWrongPasswordCount(wrongPasswordCount + 1);
            setEmail("");
            setPassword("");
        }
    }, [status, error])

    const submitHandler = async (e) => {
        e.preventDefault();
        dispatch(loginUser({
            email,
            password
        }))
    }

    return (
        <div className='container mt-3 px-5 py-5'>
            <span className="display-4">LOG IN</span>
            <form className="row needs-validation p-5" onSubmit={submitHandler}>
                <div>
                    <label htmlFor="email" className="form-label">Email</label>
                    <input type="email" name="email" id="email" className="form-control" placeholder="Email Address" value={email} onChange={(e) => setEmail(e.target.value)} required />
                    <div className="invalid-feedback">
                        Please Provide Email Address
                    </div>
                </div>
                <div>
                    <label htmlFor="password" className="form-label">Password</label>
                    <input type="password" name="password" id="password" className="form-control" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required />
                    <div className="invalid-feedback">
                        Please Provide Password
                    </div>
                </div>
                Don't have an Account. Create <Link to={"/signup"}> here </Link>.
                {wrongPasswordCount >= 2 && <div className="lead text-decoration-none"><Link to={"/"}>Forgot Password</Link></div>}
                <div className="p-2">
                    <button className="btn btn-primary my-4" type='submit' >LOG IN</button>
                </div>
            </form>

        </div>
    )
}

export default Login;