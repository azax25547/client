import { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router';
import { getASingleRecipe } from '../services/recipeService';
import { useSelector } from 'react-redux';
import { createOrder } from '../services/orderService';

const Recipe = () => {

    const [recipe, setRecipe] = useState({});
    const { id } = useParams();
    const { user, status: userStatus } = useSelector((state) => state.auth);
    const history = useHistory();

    useEffect(() => {

        const getRecipeDetails = async () => {
            const item = await getASingleRecipe(id);
            setRecipe(item?.data);
        }

        getRecipeDetails();

    }, [])
    const handleOrder = async () => {
        if (!user && userStatus !== "fulfilled") {
            alert("You need to logged in to place an Order.")
            return
        }
        let orderResponse;
        try {

            orderResponse = await createOrder({ userId: user?.user?._id, recipeId: id });
        } catch (err) {
            console.error(err);
        }

        if (orderResponse && orderResponse.status === 200) {
            alert("Order Created Succesfully");
            history.push("/orders")
        }

    }
    const { img, isAvailable, price, rating, recipeDescription, type } = recipe;
    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-6 p-4">
                        {img?.length < 0
                            ? <>
                                <img src="..." className="img-fluid" alt="Responsive Image"></img>
                            </>
                            : <>
                                <img src="https://images.pexels.com/photos/1653877/pexels-photo-1653877.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" className="img-fluid" alt="Responsive Image"></img>
                            </>}

                    </div>
                    <div className="col-6 p-4">
                        <div className={isAvailable ? "" : "text-muted"}>
                            <h2>{recipe?.recipe}</h2>
                            <p className="text-small text-muted">
                                {recipeDescription}
                            </p>
                            <p>
                                ₹ {Number(price).toFixed(2)}
                            </p>
                            <p>
                                {rating} out of 5 Stars
                            </p>
                        </div>
                        <button className="btn btn-primary" disabled={!isAvailable} onClick={() => handleOrder()}>
                            Order
                        </button>
                    </div>
                </div>

            </div>
        </>
    )
}


export default Recipe;