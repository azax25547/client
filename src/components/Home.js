import { useEffect } from "react";
import Recipes from "./Recipes";
import './home.css';
import { useDispatch, useSelector } from "react-redux";
import { allRecipes } from "../features/thunks/recipeThunk";

const Home = () => {
    const dispatch = useDispatch();
    const { recipes } = useSelector((state) => state.recipe);

    useEffect(() => {
        if (recipes.length < 1) {
            dispatch(allRecipes());
        }
    }, [])




    return <div>

        <Recipes recipes={recipes} />
    </div>
}

export default Home;