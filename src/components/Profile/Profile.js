import React, { useState } from 'react'
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { BaseDetails } from "./BaseDetails";
import MyFavorites from './MyFavorites';
import MyOrders from './MyOrders';
import MyReviews from './MyReviews';
import Settings from './Settings';

export default function Profile({ user }) {

    const [show, setShow] = useState("base");

    const { id } = useParams();


    return (
        <>
            <div className="container-fluid p-5">
                <div className="row">
                    <div className="col-md-3 col-sm-4 col-3 my-4">
                        <div className="img-header">
                            <img src="https://images.pexels.com/photos/8393919/pexels-photo-8393919.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=300&w=300" alt="profile" className="img-responsive rounded border border-5 mb-3" width="250" height="250" />
                            <h4>{user?.user?.firstname} {user?.user?.lastname}</h4>
                            <span>Joined in <strong>2014</strong></span>
                        </div>
                    </div>
                    <div className="col-md-7 col-sm-4 col-7">
                        <div className="base-details p-3 ">
                            {show === "base" && <BaseDetails heading="Profile Details" id={id} />}
                            {show === "myorders" && <MyOrders heading="My Orders" />}
                            {show === "myfav" && <MyFavorites heading="My Favorites" />}
                            {show === "myrev" && <MyReviews heading="My Reviews" />}
                            {show === "set" && <Settings heading="Profile Settings" />}
                        </div>
                    </div>
                    <div className="col-md-2 col-sm-4 col-2">
                        <div className="list-of-actions">
                            <ul className="list-group list-group-flush mt-4">
                                <button className="list-group-item list-group-item-info my-2 rounded " onClick={() => setShow("base")}><i className="fa fa-id-card" aria-hidden="true"></i>  Basic Details</button>
                                <button className="list-group-item list-group-item-info my-2 rounded " onClick={() => setShow("myorders")}><i className="fa fa-shopping-cart" aria-hidden="true"></i>  My Orders</button>
                                <button className="list-group-item list-group-item-info my-2 rounded " onClick={() => setShow("myfav")}><i className="fa fa-heart" aria-hidden="true"></i>  My Favorites</button>
                                <button className="list-group-item list-group-item-info my-2 rounded " onClick={() => setShow("myrev")}><i className="fa fa-comments" aria-hidden="true"></i>  My Reviews</button>
                                <button className="list-group-item list-group-item-info my-2 rounded " onClick={() => setShow("set")}><i className="fa fa-cog" aria-hidden="true"></i>  Settings</button>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
