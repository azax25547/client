import React from 'react'

export default function Settings() {
    return (
        <div>
            <ul className="list-group list-group-flushed">

                <li className="list-group-item">Change User Details</li>
                <li className="list-group-item">Set Preferences</li>
                <li className="list-group-item">Change Payment Options</li>
                <li className="list-group-item">Change Password</li>
                <li className='list-group-item ttext-danger'>Delete Account</li>

            </ul>
        </div>
    )
}
