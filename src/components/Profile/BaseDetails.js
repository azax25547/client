import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { getSingleUserDetails } from '../../features/thunks/userThunk';

export const BaseDetails = ({ heading, id }) => {


    const userState = useSelector(state => state.user);
    const dispatch = useDispatch();
    useEffect(() => {
        console.log(userState.userData);
        if (userState.userData.length < 1)
            dispatch(getSingleUserDetails({ id }))
    }, [])
    const { userData: u } = userState;
    return (
        <>
            <h2 className='display-3'>{heading}</h2>
            <br />

            <ul className='list-group list-group-flush'>
                <li className='list-group-item'>User First Name: {u[0]?.firstname}</li>
                <li className='list-group-item'>User last name: {u[0]?.lastname}</li>
                <li className='list-group-item'>User Email Address: {u[0]?.email}</li>
                <li className='list-group-item'>User phone Number: {u[0]?.contactNumber}</li>
                <li className='list-group-item'>User's Primary Address: {u[0]?.address[0]}</li>
            </ul>

            <a href='#'>Change User Details</a>




        </>
    )
}