import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import persistReducer from 'redux-persist/es/persistReducer';
import persistStore from 'redux-persist/es/persistStore';
import { rootPersistConfig, rootReducer } from '../rootReducer';

const store = configureStore({
    reducer: persistReducer(rootPersistConfig, rootReducer),
    middleware: getDefaultMiddleware({
        serializableCheck: false,
        immutableCheck: false
    }),
    devTools: true
})


const persistor = persistStore(store);

export { store, persistor };