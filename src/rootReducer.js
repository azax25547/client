import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";

import authSlice from "./features/auth/authSlice";
import recipeSlice from "./features/recipes/recipeSlice";
import userSlice from "./features/user/userSlice";

const rootPersistConfig = {
    key: 'root',
    storage,
    whitelist: ["auth", "recipe", "user"]
}

const rootReducer = combineReducers({
    auth: authSlice,
    recipe: recipeSlice,
    user: userSlice
})

export { rootPersistConfig, rootReducer };