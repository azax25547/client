import React from 'react';
import { Redirect, Route } from "react-router-dom";

function Private({ component: Component, ...restOfProps }) {
    const isAuthenticated = localStorage.getItem('token');
    console.log(isAuthenticated ? "s" : "x");

    return(
        <Route
            {...restOfProps}
            render={(props) => isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />}
        />
    )
}

export default Private;