import {reviewApi} from "./api";

export const getAllReviews = async (userId) => {
    return await reviewApi.get(`reviews?userId=${userId}`);
}

