import { getAuthHeader, orderApi } from "./api";

export const getOrders = async (userId) => {
    orderApi.defaults.headers.common = getAuthHeader();

    const response = await orderApi.get("/orders", {
        params: {
            userId
        }
    });
    return response;
}

export const createOrder = async (data) => {
    orderApi.defaults.headers.common = getAuthHeader();
    const response = await orderApi.post("/orders", data);
    return response;
}