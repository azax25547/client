import { authApi } from "./api";

export const login = (data) => {

    const { username, password } = data;

    const response = authApi.post("/login", {
        username,
        password
    })
    return response;
}


export const signUp = async (data) => {
    const { firstname, lastname, username, password, contact } = data;

    const response = await authApi.post("/register", {
        firstname,
        lastname,
        username,
        password,
        contact
    })

    return response;
}

export const currentUser = async () => {
    const response = await authApi.get('/');
    return response;
}