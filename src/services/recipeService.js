import { recipeApi } from "./api";

export const getAllRecipes = async () => {
    const response = await recipeApi.get("/recipes");
    return response;
}

export const getASingleRecipe = async (id) => {
    const response = await recipeApi.get(`/recipes/${id}`);
    return response;
}