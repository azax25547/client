import axios from 'axios';

const getHeaders = () => {
    return {
        "Content-Type": "Application/json"
    }
}


export const getAuthHeader = () => {
    const jwt = localStorage.getItem('token');
    if (jwt)
        return {
            "x-access-token": jwt
        }

    return {}
}



export const authApi = axios.create({
    baseURL: "/auth",
    withCredentials: false,
    headers: getHeaders()
})

export const recipeApi = axios.create({
    baseURL: "/api",
    withCredentials: false,
    headers: getHeaders()
})

export const userApi = axios.create({
    baseURL: "/user",
    withCredentials: true,
    headers: getHeaders()
})

export const orderApi = axios.create({
    baseURL: "/api",
    withCredentials: true,
    headers: getHeaders()
})

export const reviewApi = axios.create({
    baseURL: "http://localhost:4005/api",
    withCredentials: true,
    headers: getHeaders()
})

authApi.defaults.headers.common = getAuthHeader();
userApi.defaults.headers.common = getAuthHeader();
orderApi.defaults.headers.common = getAuthHeader();
reviewApi.defaults.headers.common = getAuthHeader();

