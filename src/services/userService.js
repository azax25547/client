import { userApi } from "./api";

export const getASingleUser = async (id) => {
    const response = await userApi.get(id, {
        headers: {
            "x-access-token": localStorage.getItem("token")
        }
    });
    return response;
}