import Home from "./components/Home";
import Signup from "./components/Signup";
import Login from "./components/Login";
import Recipe from "./components/Recipe";
import Order from "./components/Order";
import Dashboard from "./components/Dashboard";

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { useSelector } from "react-redux";
import Navbar from "./components/Navbar";
import Profile from "./components/Profile/Profile";
import Private from "./utils/Private";

function App() {

  const { user } = useSelector((state) => state.auth);

  return (
    <Router>
      <Navbar user={user} />
      <Switch>
        <Route path="/signup">
          <Signup />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path='/recipes/:id'>
          <Recipe />
        </Route>
        <Private path="/dashboard" component={Dashboard} />

        <Private path="/orders/" component={Order} />

        <Private path='/profile/:id' component={Profile} user={user}/>

        <Route path="/">
          <Home />
        </Route>

      </Switch>
    </Router>
  );
}

export default App;
